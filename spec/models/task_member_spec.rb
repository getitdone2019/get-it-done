require 'rails_helper'

RSpec.describe TaskMember, type: :model do
  let( :user ) { create( :user ) }
  let( :project ) { create( :project ) }
  let( :team ) { create( :team ) }
  let( :task ) { create( :task, parent: project ) }

  describe "add a user to a task" do
    context "the user is not a member of the project that the task belongs to" do
      subject( :task_member ) { build( :task_member, task: task, user: user ) }

      it { is_expected.to_not be_valid }
    end

    context "the user is a member of the project the task belongs to" do
      let( :team_member )     { build( :team_member, team: team, user: user, role: 'admin' ) }
      subject( :task_member ) { create( :task_member, task: task, user: user ) }

      before do
        project.teams << team
        team.team_members << team_member
      end

      it { is_expected.to be_valid }
    end

    context "the user has previously been removed from the task" do
      let( :team_member )     { build( :team_member, team: team, user: user, role: 'admin' ) }
      let( :task_member )     { create( :task_member, task: task, user: user ) }

      before do
        project.teams << team
        team.team_members << team_member
        task_member.save
      end

      # it "unflags the user as deleted" do
      #   expect(TaskMember.count).to eq(1)
      #   task_member.destroy
      #   expect(TaskMember.count).to eq(0)
      #   expect(TaskMember.deleted.count).to eq(1)
      #   task_member.undelete!
      #   expect(TaskMember.count).to eq(1)
      #   expect(TaskMember.deleted.count).to eq(0)
      # end
    end
  end

  describe "remove a user from a task" do
    context "there is a user assigned to a task" do
      let( :team_member )     { build( :team_member, team: team, user: user, role: 'admin' ) }
      subject( :task_member ) { create( :task_member, task: task, user: user ) }

      before do
        project.teams << team
        team.team_members << team_member
        task_member.save
      end

      # it "flags the user as deleted" do
      #   expect(TaskMember.count).to eq(1)
      #   task_member.destroy
      #   expect(TaskMember.count).to eq(0)
      #   expect(TaskMember.deleted.count).to eq(1)
      # end
    end
  end
end

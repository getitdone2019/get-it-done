require 'rails_helper'

RSpec.describe TeamMember, type: :model do
  let( :user ) { create( :user ) }
  let( :team ) { create( :team ) }
  let( :role ) { "admin" }

  describe "#new" do
    let(:valid_attributes) do
      {
        team: team,
        user: user,
        role: role
      }
    end

    let(:invalid_attributes) do
      {
        team: team,
        user: user,
        role: nil
      }
    end

    context 'when attributes are valid' do
      subject(:team_member) { build(:team_member, valid_attributes) }

      it { is_expected.to be_valid }
    end

    context 'when attributes are NOT valid' do
      subject(:team_member) { build(:team_member, invalid_attributes) }

      it { is_expected.not_to be_valid }
    end
  end

  describe "validates" do
    context "confirm relationship between users and teams" do
      subject { build(:team_member, user: user, team: team, role: role) }
      it { is_expected.to belong_to(:user) }
      it { is_expected.to belong_to(:team) }
    end

    context "valid role supplied" do
      context "role: admin" do
        subject { build(:team_member, user: user, team: team, role: role) }
        it { is_expected.to be_valid }
      end

      context "role: can_work_on_tasks" do
        let( :role ) { "can_work_on_tasks" }
        subject { build(:team_member, user: user, team: team, role: role) }
        it { is_expected.to be_valid }
      end

      context "role: can_manage_tasks" do
        let( :role ) { "can_manage_tasks" }
        subject { build(:team_member, user: user, team: team, role: role) }
        it { is_expected.to be_valid }
      end

      context "role: can_manage_team" do
        let( :role ) { "can_manage_team" }
        subject { build(:team_member, user: user, team: team, role: role) }
        it { is_expected.to be_valid }
      end
    end

    context "invalid role supplied" do
      let( :role ) { "muppet" }
      it "is supplied with a role: muppet" do
        expect { build(:team_member, user: user, team: team, role: role) }
          .to raise_error(ArgumentError).with_message(/is not a valid role/)
      end

      subject { build(:team_member, user: user, team: team) }
      it "is has no role supplied" do
        is_expected.to be_valid
      end
    end
  end

  describe "new team member creation" do
    context 'when all valid parameters are present' do
      let(:member) { build(:team_member, user: user, team: team, role: role) }
      it 'creates a new record' do
        member.save
        expect(TeamMember.count).to eq(1)
      end
    end
  end

  describe "team member deletion logic" do
    let(:team) { create(:team) }
    context "if there are many team members" do
      let(:user1) { create(:user) }
      let(:user2) { create(:user) }
      let(:user3) { create(:user) }

      context "and the team is deleted" do
        it "expects the team and all team_member rows to be deleted" do
          team.team_members.create!(user: user1, role: TeamMember.roles[:admin])
          team.team_members.create!(user: user2, role: TeamMember.roles[:admin])
          team.team_members.create!(user: user3, role: TeamMember.roles[:admin])

          expect(Team.count).to eq(1)
          expect(TeamMember.count).to eq(3)
          expect(User.count).to eq(3)

          team.destroy

          expect(Team.count).to eq(0)
          expect(TeamMember.count).to eq(0)
          expect(User.count).to eq(3)
        end
      end

      context "and there are more than 1 team admins" do
        context "and 1 is removed" do
          it "expects the team to remain and only the team member to be removed" do
            team.team_members.create!(user: user1, role: TeamMember.roles[:admin])
            team.team_members.create!(user: user2, role: TeamMember.roles[:admin])
            team.team_members.create!(user: user3, role: TeamMember.roles[:can_work_on_tasks])

            expect(Team.count).to eq(1)
            expect(TeamMember.count).to eq(3)
            expect(User.count).to eq(3)

            user2.destroy

            expect(Team.count).to eq(1)
            expect(TeamMember.count).to eq(2)
            expect(User.count).to eq(2)
          end
        end
      end

      context "and there is only 1 team admin" do
        context "who is removed" do
          it "expects the team to be deleted and all the team member links" do
            team.team_members.create!(user: user1, role: TeamMember.roles[:admin])
            team.team_members.create!(user: user2, role: TeamMember.roles[:can_work_on_tasks])
            team.team_members.create!(user: user3, role: TeamMember.roles[:can_work_on_tasks])

            expect(Team.count).to eq(1)
            expect(TeamMember.count).to eq(3)
            expect(User.count).to eq(3)

            user1.destroy

            expect(Team.count).to eq(0)
            expect(TeamMember.count).to eq(0)
            expect(User.count).to eq(2)
          end
        end
      end
    end
  end
end

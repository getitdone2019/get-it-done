require 'rails_helper'

RSpec.describe TeamProject, type: :model do
  let( :team ) { create( :team ) }
  let( :project ) { create( :project ) }

  describe '#new' do
    context 'when all valid parameters are present' do
      let(:team_project) { build(:team_project, team: team, project: project) }
      it 'creates a new record' do
        team_project.save
        expect(TeamProject.count).to eq(1)
      end
    end
  end

  describe "validates" do
    context "confirm relationship between projects and teams" do
      subject { build(:team_project, project: project, team: team) }
      it { is_expected.to belong_to(:project) }
      it { is_expected.to belong_to(:team) }
    end
  end
end

require 'rails_helper'

RSpec.describe Comment, type: :model do

  let(:user) { create(:user) }
  let(:project) { create(:project) }
  let(:task) { build(:task, parent: project) }

  describe "Create new comment" do
    context 'when all valid parameters are present' do
      let(:comment) {Comment.create(task: task, user: user, content: "testing!")}
      it 'creates a new record' do
        comment.save
        expect(Comment.count).to eq(1)
      end
    end
  end
end

require 'rails_helper'

RSpec.describe ProjectOwner, type: :model do
  let( :user ) { create( :user ) }
  let( :project ) { create( :project ) }

  describe "Create new project Owner" do
    context 'when all valid parameters are present' do
      let(:project_owner) { build(:project_owner, project: project, user: user) }
      it 'creates a new record' do
        project_owner.save
        expect(ProjectOwner.count).to eq(1)
      end
    end
  end
end

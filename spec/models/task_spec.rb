require 'rails_helper'

RSpec.describe Task, type: :model do

  let(:project) { create(:project) }
  let( :status ) { "not_started" }

  describe 'validations' do
    subject { build(:task, parent: project, status: status) }
    it { is_expected.to belong_to(:parent) }

    context "valid status" do
      context "status: not_started" do
        subject { build(:task, parent: project, status: status) }
        it { is_expected.to be_valid }
      end

      context "status: in_progress" do
        let( :status ) { "in_progress" }
        subject { build(:task, parent: project, status: status) }
        it { is_expected.to be_valid }
      end

      context "status: completed" do
        let( :status ) { "completed" }
        subject { build(:task, parent: project, status: status) }
        it { is_expected.to be_valid }
      end

      context "status: ready_for_delivery" do
        let( :status ) { "ready_for_delivery" }
        subject { build(:task, parent: project, status: status) }
        it { is_expected.to be_valid }
      end
    end
  end

  describe '#new' do
    context 'when all valid parameters are present' do
      let(:task) { build(:task, parent: project) }
      it 'creates a new record' do
        task.save
        expect(Task.count).to eq(1)
      end
    end
  end

  describe 'hierarchy' do
    let(:project1) { create(:project) }
    let(:tasks)    { create_list(:task, 2, parent: project1) }
    let(:subtask1) { create(:task, parent: tasks[0]) }
    let(:subtask2) { create(:task, parent: tasks[1]) }

    let(:valid_response) do
      {
        project1 => [
          tasks[0] => [ subtask1 ],
          tasks[1] => [ subtask2 ]
        ]
      }
    end

    context 'when there is only one level with one child' do
      let(:task_hierarchy) do
        {
          tasks[0] => [subtask2]
        }
      end

      before { tasks[0].tasks << subtask2 }

      subject { tasks[0].hierarchy }
      it 'is created correctly' do
        expect(subject).to match(task_hierarchy)
      end
    end

    context 'when there are more than one level with 3 children' do
      let(:subtask1a) { create(:task, parent: subtask1) }
      let(:subtask1b) { create(:task, parent: subtask1) }

      let(:task_hierarchy) do
        {
          tasks[0] => [
            { subtask1 => [subtask1a, subtask1b] },
            subtask2
          ]
        }
      end

      before do
        subtask1.tasks << [subtask1a, subtask1b]
        tasks[0].tasks << [subtask1, subtask2]
      end

      subject { tasks[0].hierarchy }

      it 'creates the hierarchy correctly for the multilevel structure' do
        expect(subject).to match(task_hierarchy)
      end
    end

  end

  describe 'parent_project' do
    let(:project1) { create(:project) }
    let(:task)     { create(:task, parent: project1) }
    let(:subtask1) { create(:task, parent: task) }

    context 'when the immediate parent is a Project' do
      it 'provides that project' do
        expect(task.parent_project).to eq project1
      end
    end

    context 'when the Project parent is a few higher' do
      it 'provides the correct project' do
        expect(subtask1.parent_project).to eq project1
      end
    end
  end
end

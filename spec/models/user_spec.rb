require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#can_manage? task:' do
    let(:project) { create(:project) }
    let(:task)    { create(:task, parent: project) }
    let(:subtask) { create(:task, parent: task) }

    let(:peter)   { create(:user, username: 'Peter') }
    let(:team)    { create(:team) }

    let(:task_member)  { create(:task_member, user: peter, task: subtask) }
    let(:team_member)  { create(:team_member, user: peter, team: team) }
    let(:team_project) { create(:team_project, project: project, team: team) }

    before do
      team.team_projects << team_project
      team.team_members << team_member
      peter.projects << project
      peter.task_members << task_member
    end

    subject { peter.can_manage?(task: expected_task) }

    context 'when the task is assigned to peter' do
      let(:expected_task) { subtask }
      it { is_expected.to be true }
    end

    context 'when the task is not assigned to peter' do
      let(:expected_task) { task }
      it { is_expected.to be false }
    end

  end
end

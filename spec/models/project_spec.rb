require 'rails_helper'

RSpec.describe Project, type: :model do
  describe 'has_participant?' do
    let(:team)        { create(:team) }
    let(:users)       { create_list(:user, 3) }
    let(:team_member1) { create(:team_member, user: users[0], team: team) }
    let(:team_member2) { create(:team_member, user: users[1], team: team) }
    let(:team_member3) { create(:team_member, user: users[2], team: team) }

    let(:project)      { create(:project) }
    let(:team_project) { create(:team_project, project: project, team: team) }

    before do
      project.team_projects << team_project
      team.team_members = [ team_member1, team_member2, team_member3 ]
    end

    it 'finds the correct participant' do
      expect(project.has_participant?(users[0])).to be true
    end
  end

  describe 'deletion' do
    let(:project)      { create(:project) }
    let(:users)       { create_list(:user, 2) }
    
    context 'project has no tasks' do
      it 'deletes the project' do
        project.destroy

        expect(Project.count).to eq(0)
      end
    end

    context 'project has a task' do
      let(:task) { create(:task, parent: project) }

      before do
        project.tasks << task
      end

      context 'project has a team' do
        let(:team) { create(:team)}

        before do
          project.teams << team
          team.team_members << TeamMember.create(user: users[0], team: team, role: 'admin')
        end

        context 'task has a member' do
          before do
            task.users << users[0]
          end

          it 'deletes the project and by extension the task.' do
            expect(TaskMember.count).to eq(1)
            expect(Project.count).to eq(1)
            expect(Task.count).to eq(1)
            expect(User.count).to eq(2)

            project.destroy

            expect(Project.count).to eq(0)
            expect(Task.count).to eq(0)
            # expect(Task.deleted.count).to eq(1)
            expect(TaskMember.count).to eq(0)
            # expect(TaskMember.deleted.count).to eq(1)
            expect(User.count).to eq(2)
          end
        end
      end

      context 'project has no teams' do
        context 'task has no members' do
          it 'deletes the project and by extension the task.' do
            project.destroy

            expect(Project.count).to eq(0)
            expect(Task.count).to eq(0)
          end
        end
      end
    end
  end
end

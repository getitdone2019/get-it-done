require 'rails_helper'

RSpec.describe ProjectsController, type: :controller do
  let(:valid_session) { {} }

  login_user

  describe "GET #index" do
    it "returns a success response" do
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end
end

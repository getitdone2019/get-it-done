require 'rails_helper'

RSpec.describe TeamProjectsController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Task. As you add validations to Task, be sure to
  # adjust the attributes here as well.
  let(:project) { create(:project) }
  let(:team) { create(:team) }

  let(:valid_attributes) {
    {
      project: project.id,
      team: team.id
    }
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # TasksController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  login_user

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {team: team.id, project: project.id}, session: valid_session
      expect(response).to be_successful
    end
  end
end

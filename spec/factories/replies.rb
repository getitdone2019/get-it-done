FactoryBot.define do
  factory :reply do
    comment_id { "" }
    user_id { "" }
    content { "MyText" }
    is_hidden { false }
  end
end

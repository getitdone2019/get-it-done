FactoryBot.define do
  factory :team do
    name { FFaker::Name.name }
    description { FFaker::Lorem.phrase }
  end
end

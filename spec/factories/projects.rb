FactoryBot.define do
  factory :project do
    name { FFaker::Name }
    description { FFaker::Lorem.paragraph }
  end
end

FactoryBot.define do
  factory :task, class: Task do
    name        { FFaker::Name.name}
    description { FFaker::Lorem.phrase}
    parent      { create(:project) }
  end
end

FactoryBot.define do
  factory :team_member do
    user
    team
    role { TeamMember.roles.keys.sample }
  end
end

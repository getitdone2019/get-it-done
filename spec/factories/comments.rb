FactoryBot.define do
  factory :comment do
    parent_type { "MyString" }
    parent_id { 1 }
    user_id { 1 }
    body { "MyText" }
  end
end

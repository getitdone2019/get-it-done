require 'rails_helper'

RSpec.describe "tasks/index", type: :view do
  let(:parent_1) { create(:project) }
  let(:parent_2) { create(:task, parent: parent_1) }

  before(:each) do
    assign(:tasks, [
      Task.create!(
        :name => "Name",
        :description => "Description",
        :parent_type => parent_1.class.name,
        :parent_id => parent_1.id
      ),
      Task.create!(
        :name => "Name",
        :description => "Description",
        :parent_type => parent_2.class.name,
        :parent_id => parent_2.id
      )
    ])
  end

  it "renders a list of tasks" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => Task.to_s, :count => 2
    assert_select "tr>td", :text => Task.to_s, :count => 2
  end
end

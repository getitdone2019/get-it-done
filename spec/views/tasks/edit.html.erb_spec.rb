require 'rails_helper'

RSpec.describe "tasks/edit", type: :view do
  let(:parent) { create(:project) }
  before(:each) do
    @task = assign(:task, Task.create!(
      :name => "MyString",
      :description => "MyString",
      parent_id: parent.id,
      parent_type: parent.class.name
    ))
  end

  it "renders the edit task form" do
    pending
    render

    assert_select "form[action=?][method=?]", task_path(@task), "post" do

      assert_select "input[name=?]", "task[name]"

      assert_select "input[name=?]", "task[description]"
    end
  end
end

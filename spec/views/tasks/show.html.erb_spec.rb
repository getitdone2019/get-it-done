require 'rails_helper'

RSpec.describe "tasks/show", type: :view do
  before(:each) do
    @task = assign(:task, Task.create!(
      name: "Name",
      description: "Description",
      parent: create(:project)
    ))
  end

  it "renders attributes in <p>" do
    pending
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
  end
end

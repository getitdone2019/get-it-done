require 'rails_helper'

RSpec.describe "teams/show", type: :view do
  let( :admin_user ) { create( :user) }
  let( :team_manager ) { create( :user) }
  let( :team_member ) { create( :user) }

  before(:each) do
    @team = assign(:team, Team.create!(
      name: "Name",
      description: "Description"
    ))

    # needed to set the current_user variable
    allow(view).to receive(:user_signed_in?) { true }
  end

  context "testing add team member button" do
    context "logged in user is an admin" do
      it "button should be shown" do
        allow(view).to receive(:current_user) { admin_user } 

        # Add users to the team with appropriate roles.
        @team.team_members.create(user: admin_user, role: TeamMember.roles[:admin])

        render

        expect(rendered).to include 'add-team-member-button'
      end
    end

    context "logged in user has the role: can_manage_team" do
      it "button should be shown" do
        allow(view).to receive(:current_user) { team_manager }
        @team.team_members.create(user: team_manager, role: TeamMember.roles[:can_manage_team])

        render

        expect(rendered).to include 'add-team-member-button'
      end
    end

    context "logged in user has the role: can_work_on_tasks" do
      it "button should NOT be shown" do
        allow(view).to receive(:current_user) { team_member }
        @team.team_members.create(user: team_member, role: TeamMember.roles[:can_work_on_tasks])

        render

        expect(rendered).to_not include 'add-team-member-button'
      end
    end
  end

  context "testing add project button" do
    context "logged in user has the role: can_manage_team" do
      it "button should NOT be shown" do
        allow(view).to receive(:current_user) { team_manager }
        @team.team_members.create(user: team_manager, role: TeamMember.roles[:can_manage_team])

        render

        expect(rendered).to_not include 'add-project-button'
      end
    end

    context "logged in user has the role: admin" do
      it "button should be shown" do
        allow(view).to receive(:current_user) { admin_user }
        @team.team_members.create(user: admin_user, role: TeamMember.roles[:admin])

        render

        expect(rendered).to include 'add-project-button'
      end
    end
  end
end
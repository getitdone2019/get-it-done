require 'rails_helper'

RSpec.describe "Projects", type: :request do
  let( :user ) { create( :user ) }
  let( :team1 ) {create (:team) }
  let( :team2 ) {create (:team) }
  let( :project1 ) { create( :project ) }
  let( :project2 ) { create( :project ) }
  let( :project3 ) { create( :project ) }

  before { sign_in(user) }

  describe "GET /projects" do
    it "works! (now write some real specs)" do
      get projects_path
      expect(response).to have_http_status(200)
    end

    context "The user owns 1 project" do
      it "expects to see 1 project table item" do
        user.projects << project1
        get projects_path
        assert_select '.project', count: 1
      end
    end

    context "The user owns 1 project and is connected to another through a team" do
      it "expects to see 2 project table items" do
        user.projects << project1
        user.team_members.new(team: team1, role: TeamMember.roles[:admin])
        team1.projects << project2

        get projects_path
        assert_select '.project', count: 2
      end
    end

    context "The user is in a team connected to 2 projects" do
      it "expects to see 2 project table items" do
        team1.projects << project1 << project2
        user.team_members.new(team: team1, role: TeamMember.roles[:admin])

        get projects_path
        assert_select '.project', count: 2
      end
    end

    context "The user is in a team connected to 2 projects and another team connected to 1 project" do
      it "expects to see 3 project table items" do
        team1.projects << project1 << project2
        team2.projects << project3
        user.team_members.new(team: team1, role: TeamMember.roles[:admin])
        user.team_members.new(team: team2, role: TeamMember.roles[:admin])

        get projects_path
        assert_select '.project', count: 3
      end
    end
  end
end

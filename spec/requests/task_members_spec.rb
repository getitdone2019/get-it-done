require 'rails_helper'

RSpec.describe "TaskMembers", type: :request do
  let( :user ) { create( :user ) }
  
  describe "GET /task_members" do
    before  { sign_in user }

    it "works! (now write some real specs)" do
      get task_members_path
      expect(response).to have_http_status(200)
    end
  end
end

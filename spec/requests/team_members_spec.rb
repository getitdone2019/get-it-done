require 'rails_helper'

RSpec.describe TeamMember, type: :request do
  let( :user ) { create( :user ) }
  let( :team ) { create( :team ) }

  describe "team_members/new" do
    before { sign_in(user) }

    subject { get new_team_member_path }

    before { subject }

    context 'when there are NOT team_members present' do
      let(:team) { create(:team, users: nil) }

      it { expect(response).to have_http_status(:ok) }
    end

    context 'when everything is working as expected' do
      let(:roles) { TeamMember.roles.map{|k,v| k.split("_").each(&:capitalize!).join(" ") } }
      it 'locates the endpoint' do
        roles.each do |role, v|
          expect(response.body).to include(role)
        end
      end
    end
  end

  describe "team_members POST (create)" do
    let(:team) { create(:team) }
    let(:valid_attributes) do
      {
        user_id: user.id,
        team_id: team.id,
        role:    TeamMember.roles.first[0]
      }
    end

    before  { sign_in user }

    subject { post "/team_members", params: { team_member: valid_attributes } }

    context 'when creating a new team member' do
      it 'does NOT raise any errors' do
        expect{ subject }.not_to raise_error
      end

      it 'writes a new TeamMember record' do
        expect{ subject }.to change(TeamMember, :count).by(1)
      end

      it 'redirects successfully' do
        subject
        expect(response).to have_http_status(:found)
      end
    end
  end
end

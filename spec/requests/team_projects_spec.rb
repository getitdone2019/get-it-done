require 'rails_helper'

RSpec.describe "TeamProjects", type: :request do

  before { sign_in(create(:user)) }
  
  describe "GET /team_projects" do
    it "works! (now write some real specs)" do
      get team_projects_path
      expect(response).to have_http_status(200)
    end
  end
end

module TasksHelper
  def parent_path
    method_name = @task.parent_type.downcase + "_path"
    send(method_name, @parent)
  end

  # Retrieves the amount of incomplete subtasks
  def incomplete_subtasks task
    return false if ['not_started', 'in_progress'].include?(params[:task][:status])
    task.tasks.where(status: Task.statuses[:not_started]).or(task.tasks.where(status:   Task.statuses[:in_progress]))
  end

  # checks to see if all subtasks are completed
  def completed_subtasks task
    return false if task.tasks.count < 1
    task.tasks.where(status: Task.statuses[:completed]).count == task.tasks.count
  end
end

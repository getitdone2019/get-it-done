module StaticPagesHelper
  def welcome_message
    message = "Welcome "
    message += "back " if current_user.sign_in_count > 1
    message += current_user.email
  end

  def calculate_task_tree(project:)
    hierarchy = project.tasks.map(&:hierarchy)

    markup = ""
    hierarchy.each do |level|
      markup += handle_level(level)
    end
    markup
  end

  def handle_level(level)
    markup = ""

    keys = level.keys
    keys.each do |key|
      markup += "<li><span class='caret caret-text'>#{ key.try(:name) }</span>"
      markup += "<ul class='nested'>"

      row = level[key]
      markup += handle_children(row)

      markup += "</ul>"
      markup += "</li>"
    end

    markup
  end

  def handle_children(row)
    markup = ""
    
    if keys? row # this is a hash
      markup += handle_level(row)
    else # this is an array
      row.each do |val|
        markup += keys?(val) ? handle_level(val) : "<li>#{val.name}</li>"
      end
    end

    markup
  end

  private
  def keys?(val)
    val.is_a? Hash
  end
end

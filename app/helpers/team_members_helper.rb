module TeamMembersHelper
  def users_for_selection
    User.all - team_users
  end

  def team_users
    @team.present? ? @team.users : []
  end

  def roles_for_select
    TeamMember.roles.map{|k, v| [capitalize_name(k), k] }
  end
end

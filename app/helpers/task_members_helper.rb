module TaskMembersHelper
  def show_delete_action_for(member:, user:) 
    user.can_manage?(task: member.task) || member.user == user
  end
end

module ApplicationHelper
  def project_name
    "Get it Done!"
  end

  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = project_name
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def user_is_project_owner? project
    project.project_owners.find_by(user_id: current_user.id).present?
  end

  def get_user_role user, team
    team.team_members.find_by(user_id: user.id).role
  end

  def user_is_team_admin(user:, team:)
    # get the role
    role = TeamMember.roles[team.team_members.find_by(user_id: user.id).role]

    # return if role is greater than the manage team enum.
    role == TeamMember.roles[:admin]
  end

  def user_can_manage_team(user:, team:)
    # get the role
    role = TeamMember.roles[team.team_members.find_by(user_id: user.id).role]

    # return if role is greater than the manage team enum.
    role >= TeamMember.roles[:can_manage_team]
  end

  def user_can_manage_task?(user:, task:)
    task.parent_project.can_add_task? user
  end

  def user_is_team_member? team
    team.users.find_by(id: current_user.id).present?
  end

  def capitalize_name(name)
    name.split("_").each(&:capitalize!).join(" ")
  end
end

class StaticPagesController < ApplicationController
  def home
    @projects = user_projects
    @tasks    = current_user.tasks.all
  end

  private
    def user_projects
      # Get all projects associated with the user.
      project_list = current_user.projects

      # Add all projects connected to the teams a user is in.
      current_user.teams.each do |team|
        project_list += team.projects
      end

      # Remove duplicates
      project_list.uniq
    end
end

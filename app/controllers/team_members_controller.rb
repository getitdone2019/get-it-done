class TeamMembersController < ApplicationController
  before_action :set_team_member, only: [:show, :update, :edit, :destroy]

  # GET /team_members
  # GET /team_members.json
  def index
    @team_members = TeamMember.all
  end

  # GET /team_members/1
  # GET /team_members/1.json
  def show
  end

  # GET /team_members/new
  def new
    @team_member = TeamMember.new
    @team = Team.find(params[:team_id]) if params[:team_id].present?

    respond_to do |format|
      if ! available_members?
        format.js { render template: 'shared/error', locals: {
          title: 'Cannot add a member',
          message: 'There are no users available to be added to a team'
        }}
        flash.now[:alert] = 'There are no users available to be added to a team'
      end

      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Add a Team Member',
        form: 'team_members/form'
      }}
    end
  end

  # GET /team_members/1/edit
  def edit
    respond_to do |format|
      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Edit Member',
        form: 'team_members/form'
      }}
    end
  end

  # POST /team_members
  # POST /team_members.json
  def create
    @team_member = TeamMember.new(team_member_params)

    respond_to do |format|
      if @team_member.save
        flash[:notice] = 'Team member was successfully created.';
        format.html { redirect_to team_path(@team_member.team_id) }
        format.json { render :show, status: :created, location: @team_member }
        format.js  { render template: 'shared/reload' }
      else
        format.html { render :new}
        format.json { render json: @team_member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /team_members/1
  # PATCH/PUT /team_members/1.json
  def update
    respond_to do |format|
      if @team_member.update(team_member_params)
        flash[:notice] = 'Team member was successfully updated.';
        format.html { redirect_to team_path(@team_member.team_id) }
        format.json { render :show, status: :ok, location: @team_member }
        format.js  { render template: 'shared/reload' }
      else
        format.html { render :edit }
        format.json { render json: @team_member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /team_members/1
  # DELETE /team_members/1.json
  def destroy
    @team_member.destroy

    respond_to do |format|
      format.html { redirect_to target_url, notice: 'Team member was successfully removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team_member
      @team_member = TeamMember.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def team_member_params
      params.require(:team_member).permit(:user_id, :team_id, :role)
    end

    # If the member being removed is the current_user then they have removed themselves from the team.
    def target_url
      @team_member.user_id == current_user.id ? root_url : team_path(@team_member.team.id)
    end

    def available_members?
      @team.present? && @team.team_members.count != User.all.count
    end
end

class RepliesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_comment, only: [:index, :new, :create]
  before_action :find_reply, only: [:index, :update, :destroy]

  def index
    @replies = Reply.all
  end

  def create
    @reply = @comment.replies.new comment_params
    @reply.user = current_user
    respond_to do |format|
      if @reply.save
        format.html { redirect_back(fallback_location: root_path) }
        format.js
      else
        format.html { redirect_back(fallback_location: root_path) }
        format.js
      end
    end
  end

  def update
    @reply.update(is_hidden: false)
    redirect_back(fallback_location: root_path)
  end

  def destroy
    @reply.update(is_hidden: true)
    redirect_back(fallback_location: root_path)
  end

  private
    def set_comment
      @comment = Comment.find(params[:comment_id])
    end

    def find_reply
      @reply = Reply.find(params[:id])
    end

    def comment_params
      params.require(:reply).permit(:comment_id, :user_id, :content)
    end
end

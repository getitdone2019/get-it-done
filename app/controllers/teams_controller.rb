class TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]

  breadcrumb 'Teams', :teams_path, match: :exact

  # GET /teams
  # GET /teams.json
  def index
    @teams = current_user.teams
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
    breadcrumb @team.name, team_path(@team), match: :exact
  end

  # GET /teams/new
  def new
    @team = Team.new

    respond_to do |format|
      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Create New Team',
        form: 'teams/form'
      }}
    end
  end

  # GET /teams/1/edit
  def edit
    @team_projects = @team.team_projects.empty? ? [TeamProject.new] : @team.team_projects

    respond_to do |format|
      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Edit Team',
        form: 'teams/form'
      }}
    end
  end

  # POST /teams
  # POST /teams.json
  def create
    @team = Team.new(team_params)

    # Need to connect the new team to the user who has created it.
    @team.team_members << TeamMember.new(user: find_user, team: @team, role: 'admin')

    # Attempt to get the project if one was selected.
    @team.projects << Project.find(params[:team][:project_ids]) if params[:team][:project_ids].present?

    respond_to do |format|
      if @team.save
        flash[:notice] = 'Team was successfully created.'
        format.html { redirect_to @team }
        format.json { render :show, status: :created, location: @team }
        format.js { render template: 'shared/redirect', locals: {path: team_path(@team) } }
      else
        format.json { render json: @team.errors.full_messages, status: :unprocessable_entity }
        flash.now[:alert] = 'A problem was encountered creating the team!'
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    respond_to do |format|
      if @team.update(team_params)
        flash[:notice] = 'Team was successfully updated.'
        format.html { redirect_to @team, notice: 'Team was successfully updated.' }
        format.json { render :show, status: :ok, location: @team }
        format.js  { render template: 'shared/reload' }
      else
        format.json { render json: @team.errors.full_messages, status: :unprocessable_entity }
        flash.now[:alert] = 'A problem was encountered updating the team!'
        format.html { render :edit }
      end
    end
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: 'Team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    def find_user
      User.find(params[:team][:user])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(
                                      :name,
                                      :description)
    end
end

class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  breadcrumb 'Projects', :projects_path, match: :exact

  # GET /projects
  # GET /projects.json
  def index
    @projects = user_projects
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    @tasks = @project.tasks
    breadcrumb @project.name, project_path(@project.id), match: :exact
  end

  # GET /projects/new
  def new
    @project = Project.new

    respond_to do |format|
      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Create New Project',
        form: 'projects/form'
      }}
    end
  end

  # GET /projects/1/edit
  def edit
    @team_projects = @project.team_projects.empty? ? [TeamProject.new] : @project.team_projects

    respond_to do |format|
      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Edit Project',
        form: 'projects/form'
      }}
    end
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)
    respond_to do |format|
      if @project.save
        flash[:notice] = 'Project was successfully created.'
        format.html { redirect_to @project }
        format.json { render :show, status: :created, location: @project }
        format.js { render template: 'shared/redirect', locals: {path: project_path(@project) } }
      else
        format.json { render json: @project.errors.full_messages, status: :unprocessable_entity }
        flash.now[:alert] = 'A problem was encountered creating the project!'
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        flash[:notice] = 'Project was successfully updated.'
        format.html { redirect_to @project }
        format.json { render :show, status: :ok, location: @project }
        format.js  { render template: 'shared/reload' }
      else
        format.json { render json: @project.errors.full_messages, status: :unprocessable_entity }
        flash.now[:alert] = 'A problem was encountered updating the project!'
        format.html { render :edit }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params[:project].merge!({ project_owners_attributes: [user_id: current_user.id] })
      params.require(:project).permit(
        :name,
        :description,
        project_owners_attributes: [:user_id]
      )
    end
end

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  # Using in team_member controller and team/show view.
  helper_method :last_team_admin

  breadcrumb 'Home', :root_path

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username])
  end

  def user_projects
    # Get all projects associated with the user.
    @projects = current_user.projects

    # Add all projects connected to the teams a user is in.
    current_user.teams.each do |team|
      @projects += team.projects
    end

    # Remove duplicates
    @projects = @projects.uniq
  end
end

class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_task, only: [:index, :new, :create]
  before_action :find_comment, only: [:index, :update, :destroy, :refresh_comments]

  def index
    @comments = Comment.all
  end

  def create
    @comment = @task.comments.new comment_params
    @comment.user = current_user
    respond_to do |format|
      if @comment.save
        format.html { redirect_back(fallback_location: root_path) }
        format.js
      else
        format.html { redirect_back(fallback_location: root_path) }
        format.js
      end
    end
  end

  def update
    @comment.update(is_hidden: false)
    redirect_back(fallback_location: root_path)
  end

  def destroy
    @comment.update(is_hidden: true)
    redirect_back(fallback_location: root_path)
  end

  private
    def set_task
      @task = Task.find(params[:task_id])
    end

    def find_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:task_id, :user_id, :content)
    end
end

class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :create, :update, :destroy]
  before_action :set_parent, only: [:new, :show, :edit]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = Task.all
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
    # Get once here rather than multiple times in the view.
    @can_manage_task = current_user.can_manage?(task: @task)

    build_task_breadcrumb
  end

  # GET /tasks/new
  def new
    @task = Task.new(parent: @parent)

    respond_to do |format|
      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Create New Task',
        form: 'tasks/form'
      }}
    end
  end

  # GET /tasks/1/edit
  def edit
    respond_to do |format|
      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Edit Task',
        form: 'tasks/form'
      } }
    end
  end

  # POST /tasks
  # POST /tasks.json
  def create
    respond_to do |format|
      if @task.save
        flash[:notice] = 'Task was successfully created.';
        format.html { redirect_to controller: controller, action: :show, id: task_params[:parent_id] }
        format.json { render :show, status: :created, location: @task }
        format.js { render template: 'shared/redirect', locals: {path: task_path(@task) } }
      else
        format.json { render json: @task.errors.full_messages, status: :unprocessable_entity }
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    if status_changed.present?
      if helpers.incomplete_subtasks(@task).present?
        flash[:notice] = "#{@task.name} cannot progress until all subtasks are completed"
        redirect_to task_path(@task.id)
      else
        if ['ready_for_delivery'].include?(params[:task][:status])
          update_status
          set_subtasks
        else
          update_status
        end
      end
    else
      update_task
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to controller: controller, action: :show, id: @task.parent, notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task
      @task = task_from_id || task_from_params
    end

    def set_parent
      # if the params contains a type key then the current task is being created and doesn't exist yet.
      @parent = params.has_key?(:parent_type) ? params[:parent_type].constantize.find(params[:parent_id]) : @task.parent
    end

    # Use to set the corrent redirection controller for task edit update.
    def controller
      @task.parent_type.downcase.pluralize.to_sym
    end

    def task_from_id # show
      Task.find(params[:id]) if params[:id].present?
    end

    def task_from_params # create
      Task.new(task_params) unless @task
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:name, :description, :parent_id, :parent_type, :status)
    end

    # Tasks must build a heirarchy of tasks based on a project.
    def build_task_breadcrumb
      # Set the initial crumb.
      breadcrumb 'Projects', :projects_path, match: :exact

      previous_breadcrumb @task

      # Add current crumb to the end.
      breadcrumb @task.name, task_path(@task), match: :exact
    end

    # Recursively work up the hierarchy to build trail.
    def previous_breadcrumb task
      parent = task.parent
      if parent.is_a? Project
        breadcrumb parent.name, project_path(parent), match: :exact
      else
        # Go deeper.
        previous_breadcrumb parent
        breadcrumb parent.name, task_path(parent), match: :exact
      end
    end

    # if a task is being updated through the edit_task_path view
  def update_task
    respond_to do |format|
      if @task.update(task_params)
        flash[:notice] = 'Task was successfully updated.';
        format.html { redirect_to task_path, action: :show, id: @task.parent }
        format.json { render :show, status: :ok, location: @task }
        format.js  { render template: 'shared/reload' }
      else
        render_error :edit
      end
    end
  end

  # if a task status is being updated through the status buttons provided on the task_path view
  def update_status
    respond_to do |format|
      if @task.update(task_params)
        flash[:notice] = "#{@task.name} has successfully transitioned to #{helpers.capitalize_name @task.status}"
        format.html { redirect_to task_path(@task.id), action: :show, id: @task.parent }
        format.json { render :show, status: :ok, location: @task }
        format.js  { render template: 'shared/reload' }
      else
        render_error :edit
      end
    end
  end

  # sets all subtasks to ready_for_delivery if parent task is transitioning to 'ready_for_delivery'
  def set_subtasks
    @task.tasks.where(status: "completed").find_each { |u| u.update(status: "ready_for_delivery") }
  end

  # Checks to see if the status of a task has been changed
  def status_changed
    @task.status != params[:task][:status]
  end

  def render_error target
    format.json { render json: @task.errors, status: :unprocessable_entity }
    format.html { render target }
  end
end

class TaskMembersController < ApplicationController
  before_action :set_task_member, only: [:destroy]

  # GET /task_members/new
  def new
    @task_member = TaskMember.new
    @task = Task.find_by(id: params[:task]);

    # If user is not nil then the user is trying to add themselves.
    @user = User.find_by(id: params[:user]) if params[:user].present?
    @candidate_members = @task.parent_project.users - @task.users

    respond_to do |format|
      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Add Member to Task',
        form: 'task_members/form'
      }}
    end
  end

  # POST /task_members
  # POST /task_members.json
  def create
    # Check if the task member has been removed at some point.
    # @task_member = TaskMember.deleted.find_by(task_id: task_member_params[:task_id], user_id: task_member_params[:user_id])

    # if @task_member.present?
    #   undelete
    # else
      create_new
    # end
  end

  # DELETE /task_members/1
  # DELETE /task_members/1.json
  def destroy
    @task_member.destroy

    respond_to do |format|
      flash[:notice] = 'Task member was successfully destroyed.'
      format.js
      format.html { redirect_to task_members_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task_member
      @task_member = TaskMember.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_member_params
      params.require(:task_member).permit(:task_id, :user_id)
    end

    # Object does not exist in the database. Create a new object.
    def create_new
      @task_member = TaskMember.new(task_member_params)

      respond_to do |format|
        if @task_member.save
          format.json { head :no_content }
          format.js
        else
          format.json { render json: @task_member.errors.full_messages, status: :unprocessable_entity }
        end
      end
    end

    # Object already exists. Set the delete flag to false.
    def undelete
      respond_to do |format|
        if @task_member.undelete!
          format.json { head :no_content }
          format.js
        else
          format.json { render json: @task_member.errors.full_messages, status: :unprocessable_entity }
        end
      end
    end
end

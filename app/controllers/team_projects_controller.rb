class TeamProjectsController < ApplicationController
  before_action :set_team_project, only: [:show, :edit, :update, :destroy]

  # GET /team_projects
  # GET /team_projects.json
  def index
    @team_projects = TeamProject.all
  end

  # GET /team_projects/1
  # GET /team_projects/1.json
  def show
  end

  # GET /team_projects/new
  def new
    @team_project = TeamProject.new
    @project = project_from_id
    @team = team_from_id

    respond_to do |format|
      # need to check if there are teams available to be added from a project.
      if available_teams?
        # There are no teams available to be added. redirect with an error message.
        format.js { render template: 'shared/error', locals: {
          title: 'Cannot add a team',
          message: 'There are no projects available to join this team.'
        }}
        flash.now[:alert] = 'There are no projects available to join this team.'
      end

      # ind if there are teams available for a project.
      if available_projects?
        # There are no teams available to be added. redirect with an error message.
        format.js { render template: 'shared/error', locals: {
          title: 'Cannot add a project',
          message: 'There are no projects available to assign to this team.' 
        }}
        flash.now[:alert] = 'There are no projects available to assign to this team.'
      end

      format.html
      format.js { render template: 'shared/modal_form', locals: {
        title: 'Add Team to Project',
        form: 'team_projects/form'
      }}
    end
  end

  # GET /team_projects/1/edit
  def edit
  end

  # POST /team_projects
  # POST /team_projects.json
  def create
    @team_project = TeamProject.new(team_project_params)
    @redirect = redirect_params

    respond_to do |format|
      if @team_project.save
        flash[:notice] = 'Team project was successfully created.';
        format.html { redirect_to controller: @redirect[:controller], action: :show, id: @redirect[:id] }
        format.json { render :show, status: :created, location: @team_project }
        format.js  { render template: 'shared/reload' }
      else
        format.json { render json: @team_project.errors.full_messages, status: :unprocessable_entity }
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /team_projects/1
  # PATCH/PUT /team_projects/1.json
  def update
    respond_to do |format|
      if @team_project.update(team_project_params)
        format.html { redirect_to @team_project, notice: 'Team project was successfully updated.' }
        format.json { render :show, status: :ok, location: @team_project }
        format.js  { render template: 'shared/reload' }
      else
        format.json { render json: @team_project.errors, status: :unprocessable_entity }
        format.html { render :edit }
      end
    end
  end

  # DELETE /team_projects/1
  # DELETE /team_projects/1.json
  def destroy
    @team_project.destroy
    respond_to do |format|
      format.html { redirect_to request.referer, notice: 'Team project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team_project
      @team_project = TeamProject.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_project_params
      params.require(:team_project).permit(:team_id, :project_id)
    end

    def project_from_id
      Project.find(params[:project]) if params[:project].present?
    end

    def team_from_id
      Team.find(params[:team]) if params[:team].present?
    end

    def redirect_params
      # Controller woll be set by the redirect parameter
      controller = params[:team_project][:referrer_type].downcase.pluralize.to_sym

      # THe other 2 parameters sent will be the project_id and team_id. Choose one to be redirected to.
      id = controller == :projects ? params[:team_project][:project_id] : params[:team_project][:team_id]

      return {
        controller: controller,
        id: id
      }
    end

    def available_teams?
      @project.present? && @project.teams.count == Team.all.count
    end

    def available_projects?
      @team.present? && @team.projects.count == Project.all.count
    end
end

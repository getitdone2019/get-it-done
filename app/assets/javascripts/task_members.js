$(document).on('ajax:error', function(event) {
    // Get errors from the sent data.
    var errors = event.detail[0];

    // Create an error dialog. Possibly a better way to to this.
    var er ='<div class="alert alert-danger alert-dismissable" role="alert">' +
            '  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>' +
            '    <ul>';

    for(var i = 0; i < errors.length; i++){
        var list = errors[i];
        er += "<li>"+list+"</li>"
    }
    
    er += '      </ul>' +
          '    </div>';

    // Remove any previous alerts
    $("#modal-window form alert").remove();

    // Add to the top of the form.
    $("#modal-window form").prepend($(er)); 
}).on('turbolinks:load', function() {
    $('.user-icon').each( function() {
        addPopoverEvent($(this));
        addTooltipEvent($(this));
    });

    // Close any open popovers if the user clicks somewhere outside of the menu.
    $(document).click(function(event) {
        // Check to see if the current element is outside the user icon.
        if($(event.target).closest('.icon-container').length === 0) {
            closeAllPopovers();
        }
    });
});

function closeAllPopovers() {
    $('[data-toggle="popover"].active').popover('toggle');
}

function addTooltipEvent(element) {
    element.find('[data-toggle="tooltip"]').popover();
}

function addPopoverEvent(element) {
    element.popover();
    element.on('show.bs.popover', function () {
        // Close all other popovers.
        closeAllPopovers();
        $(this).addClass('active');
    }).on('hidden.bs.popover', function () {
        $(this).removeClass('active');
    });
}
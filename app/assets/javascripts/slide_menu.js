$(document).on('turbolinks:load', function() {
  // Set the left attribute of the menu to hide it on page load.
  $('.slide-menu').each(function() {
    var width = $(this).find('.menu-items').width();
    $(this).find('.menu-container').css({
      'left': width + 5
    });

    // Set visibility. If page load is slow the menu can show for a while before hidden.
    $(this).css({
      visibility: 'visible'
    })
  });

  // Close any open memus if the user clicks somewhere outside of the menu.
  $(document).click(function(event) {
    // Check to see if the current element is outside the card menu.
    if($(event.target).closest('.slide-menu').length === 0) {
      closeAllMenus();
    }
  });

  // If the card memu toggle is clicked close or open the menu based on the current state.
  $('.slide-menu .menu-toggle').click(function (event) {
    if ($(this).hasClass('open')) {
      closeMenu($(this))
    } else {
      // Close other open menus. Comment out to disable.
      closeAllMenus();

      openMenu($(this))
    }
  });
});

function closeAllMenus() {
  $('.slide-menu .open').each(function() {
    closeMenu($(this));
  });
}

var menuAnimationDuration = 200;

function openMenu(menuButton) {
  // Slide open.
  menuButton.parent('.menu-container').animate({
    left: 0
  }, menuAnimationDuration);

  // Icon transition.
  menuButton.find('.glyphicon-menu-hamburger').css({
    opacity: 0
  });

  menuButton.find('.glyphicon-remove').css({
    display: 'initial'
  });

  menuButton.addClass('open');
}

function closeMenu(menuButton) {
  // Slide closed.
  var width = menuButton.siblings('.menu-items').width();
  menuButton.parent('.menu-container').animate({
    left: width + 5
  }, menuAnimationDuration);

  // Icon transition.
  menuButton.find('.glyphicon-menu-hamburger').css({
    opacity: 1
  });

  menuButton.find('.glyphicon-remove').css({
    display: 'none'
  });

  menuButton.removeClass('open');
}

class UserNotifierMailer < ApplicationMailer
  # send a signup email to the user, pass in the user object that   contains the user's email address

  URL = {
    development: 'localhost:3000',
    test:        'localhost:3000',
    production:  'https://get-it-done-19.herokuapp.com'
  }

  def confirmation_email(user)
    @user = user
    @link = URL[Rails.env.to_sym]

    mail(
      :to => @user.email,
      :subject => 'Thanks for signing up for our amazing app'
    )
  end
end

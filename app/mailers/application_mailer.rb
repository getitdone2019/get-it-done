class ApplicationMailer < ActionMailer::Base
  default from: 'gid.team.2019@gmail.com'
  layout 'mailer'
end

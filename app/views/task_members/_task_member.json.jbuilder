json.extract! task_member, :id, :task_id, :user_id, :created_at, :updated_at
json.url task_member_url(task_member, format: :json)

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable

  has_many  :team_members
  has_many  :teams, through: :team_members, dependent: :destroy

  has_many  :project_owners
  has_many  :projects, through: :project_owners, dependent: :destroy

  has_many :comments
  has_many :replies

  has_many :task_members
  has_many :tasks, through: :task_members

  after_create :send_confirmation_mail

  def can_manage?(task:)
    tasks.include? task
  end

  private
  def send_confirmation_mail
    UserNotifierMailer.confirmation_email(self).deliver
  end
end

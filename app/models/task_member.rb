class TaskMember < ApplicationRecord
  # include Tombstoneable

  belongs_to :user
  belongs_to :task

  validates_uniqueness_of :user_id, scope: :task
  validate :user_can_join_task

  # Will return true if the user is in a team connected to a project.
  def user_can_join_task
    unless task.parent_project.has_participant? user
      errors.add(:user, "must be in a team attached to the project this task is a part of.")
    end
  end
end

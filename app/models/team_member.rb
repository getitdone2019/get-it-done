class TeamMember < ApplicationRecord
  belongs_to :user
  belongs_to :team

  validates_uniqueness_of :user_id, scope: :team
  validates :role, presence: true

  enum role: [:can_work_on_tasks, :can_manage_tasks, :can_manage_team, :admin]

  before_destroy :destroy_team?

  def is_last_team_admin?
    num_of_admins = team.team_members.where(role: TeamMember.roles[:admin]).count

    # returns true if there is only 1 admin in the team and this member is an admin.
    num_of_admins == 1 && TeamMember.roles[role] == TeamMember.roles[:admin]
  end

  private
    def destroy_team?
      team.destroy if is_last_team_admin?
    end
end

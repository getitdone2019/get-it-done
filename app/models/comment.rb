class Comment < ApplicationRecord
  belongs_to :task
  belongs_to :user

  has_many :replies

  default_scope { order(created_at: :desc) }

  validates  :content, presence: true
  validates :user, presence: true
end

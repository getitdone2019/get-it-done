class Team < ApplicationRecord
  has_many  :team_projects
  has_many  :projects, through: :team_projects, dependent: :destroy
  accepts_nested_attributes_for  :team_projects, allow_destroy: true

  # if a team is destroyed remove all team members.
  has_many  :team_members
  has_many  :users, through: :team_members, dependent: :destroy

  validates  :name, presence: true, length: { maximum: 256 }

  # Stop a team to have the same name as another.
  validates_uniqueness_of :name

  def has_participant? user
    users.include? user
  end
end

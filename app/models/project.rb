class Project < ApplicationRecord
  # include Tombstoneable

  has_many  :project_owners
  has_many  :users, as: :owners, through: :project_owners, dependent: :destroy

  has_many   :tasks, as: :parent, dependent: :destroy

  has_many   :team_projects
  has_many   :teams, through: :team_projects, dependent: :destroy

  accepts_nested_attributes_for :project_owners, allow_destroy: true

  validates  :name, presence: true, length: { maximum: 256 }

  # Check if a user is in a team connected to the project.
  def has_participant? user
    teams.any?{|team| team.has_participant? user }
  end

  def users
    users = []

    teams.each do |team|
      users += team.users
    end

    users.uniq
  end

  def can_add_task? user
    teams.each do |team|
      team.team_members.each do |member|
        return true if member.user == user && TeamMember.roles[member.role] >= TeamMember.roles[:can_manage_tasks]
      end
    end

    return false
  end
end

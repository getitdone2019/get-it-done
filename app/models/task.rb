class Task < ApplicationRecord
  include TasksHelper
  # include Tombstoneable

  belongs_to :parent, polymorphic: true
  has_many :tasks, as: :parent, dependent: :destroy

  has_many :comments, dependent: :delete_all

  has_many :task_members
  has_many :users, through: :task_members, dependent: :destroy

  enum status: [:not_started, :in_progress, :completed, :ready_for_delivery]

  validates  :name, presence: true, length: { maximum: 64 }

  def hierarchy
    list = Hash.new {|hash, key| hash[key] = [] }

    tasks.each_with_object(list) do |obj, memo|
      memo[self] << hierarchy_value(object: obj)
      memo
    end
  end

  # Retrieves the current task status
  def get_current_status
    Task.statuses[self.status]
  end

  # Retrieves the preceeding task status
  def status_recede
    Task.statuses.key(get_current_status - 1)
  end

  # Retrieves the succeeding task status
  def status_proceed
    Task.statuses.key(get_current_status + 1)
  end

  # Checks to see if a parent task is transitioning to 'Ready for delivery' status
  def allow_status_progression
    ['ready_for_delivery'].include?(status_proceed) && (completed_subtasks(self))
  end

  def parent_project(current_parent: nil)
    current_parent = parent if current_parent.nil?

    if current_parent.is_a? Project
      current_parent
    else
      parent_project(current_parent: current_parent.parent)
    end
  end

  def has_participant? user
    users.include? user
  end

  private
    def hierarchy_value(object:)
      object.tasks.empty? ? object : object.hierarchy
    end
end

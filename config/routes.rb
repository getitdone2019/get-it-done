Rails.application.routes.draw do
  root 'static_pages#home'
  get 'static_pages/home'

  resources :task_members
  resources :team_members
  resources :team_projects
  resources :teams
  resources :projects
  devise_for :users
  resources :tasks do
    resources :comments
  end

  resources :comments do
    resources :replies
  end
end

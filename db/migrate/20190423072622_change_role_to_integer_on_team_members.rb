class ChangeRoleToIntegerOnTeamMembers < ActiveRecord::Migration[5.1]
  def up
    remove_column :team_members, :role
    add_column :team_members, :role, :integer
  end

  def down
    change_column :team_members, :role, :string
  end
end

class RemoveProjectFromTasks < ActiveRecord::Migration[5.1]
  def change
    remove_reference :tasks, :project, foreign_key: true
  end
end

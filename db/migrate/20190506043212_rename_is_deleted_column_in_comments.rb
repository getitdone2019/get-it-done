class RenameIsDeletedColumnInComments < ActiveRecord::Migration[5.1]
  def change
    change_table :comments do |t|
      t.rename :is_deleted?, :is_deleted
    end
  end
end

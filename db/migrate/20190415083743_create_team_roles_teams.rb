class CreateTeamRolesTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :team_roles_teams do |t|
      t.references :team, index: true
      t.references :team_role, index: true

      t.timestamps
    end
  end
end

class RemoveTasksFromTasks < ActiveRecord::Migration[5.1]
  def change
    remove_reference :tasks, :tasks, foreign_key: true
  end
end

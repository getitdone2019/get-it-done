class AddDeleteFlagToComments < ActiveRecord::Migration[5.1]
  def change
    add_column :comments, :is_deleted?, :boolean, null: false, default: false
  end
end

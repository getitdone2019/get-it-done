class RemoveRoleFromTeam < ActiveRecord::Migration[5.1]
  def up
    drop_table :team_roles
    drop_table :team_roles_teams 
  end
end

class AlterTypeOnTasks < ActiveRecord::Migration[5.1]
  def change
    rename_column :tasks, :type, :parent_type
  end
end

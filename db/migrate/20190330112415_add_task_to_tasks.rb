class AddTaskToTasks < ActiveRecord::Migration[5.1]
  def change
    add_reference :tasks, :tasks, foreign_key: true, index: true
  end
end

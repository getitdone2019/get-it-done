class ChangeCommentColumnNames < ActiveRecord::Migration[5.1]
  def change
    change_table :comments do |c|
      c.rename :parent_type, :commentable_type
      c.rename :parent_id, :commentable_id
    end
  end
end

class AddDeletedToProject < ActiveRecord::Migration[5.1]
  def change
    add_column :projects, :deleted, :boolean, null: false, default: false
  end
end

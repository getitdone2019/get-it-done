class DeleteCommentableInComments < ActiveRecord::Migration[5.1]
  def change
    change_table :comments do |t|
      t.rename :commentable_id, :task_id
    end
    remove_column :comments, :commentable_type
  end
end
